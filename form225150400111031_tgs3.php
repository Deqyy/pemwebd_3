<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 3 PHP</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <h2>FORM TUGAS PHP</h2>
    <div id="formResult"></div>

    <form id="myForm" method="post" action="process.php">
        <label for="name">Nama:</label><br>
        <input type="text" id="name" name="name"><br>

        <label for="gender">Jenis Kelamin:</label><br>
        <input type="radio" id="male" name="gender" value="Laki - Laki">
        <label for="male">Laki - Laki</label><br>
        <input type="radio" id="female" name="gender" value="Wanita">
        <label for="female">Wanita</label><br>
        
        <label for="city">Kota Asal:</label><br>
        <select id="city" name="city">
            <option value="Jakarta">Jakarta</option>
            <option value="Malang">Malang</option>
            <option value="Denpasar">Denpasar</option>
            <option value="Tangerang">Tangerang</option>
            <option value="Banyuwangi">Banyuwangi</option>
            <option value="Riau">Riau</option>
            <option value="Ngawi">Ngawi</option>

        </select><br>

        <input type="submit" id="submitBtn" value="Submit">
    </form>

    <script>
        $(document).ready(function(){
            $("#myForm").submit(function(e){
                e.preventDefault(); 

                var name = $("#name").val();
                var gender = $("input[name='gender']:checked").val();
                var city = $("#city").val();

                $.ajax({
                    type: "POST",
                    url: "process.php",
                    data: {name: name, gender: gender, city: city},
                    success: function(response){
                        $("#formResult").html(response);
                    }
                });
            });
        });
    </script>
</body>
</html>
